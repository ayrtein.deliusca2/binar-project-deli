'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     */ Example:
     await queryInterface.addColumn('students', 
      'password',
      Sequelize.INTEGER);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     */ Example:
     await queryInterface.dropColumn('students', 'password');
     
  }
};

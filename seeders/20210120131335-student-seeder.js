'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
      await queryInterface.bulkInsert('students', 
      [
        {
        name: 'Ronaldo',
        email: 'ronaldo',
        school_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Zidane',
        email: 'Zidane',
        school_id: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Raul',
        email: 'raul',
        school_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    
    ], {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     await queryInterface.bulkDelete('students', null, {});
     */
  }
};

const activity = require('../models').activity;
const Student = require('../models').student;

const bcrypt = require("bcrypt");

module.exports = {
    async getStudentlist(req, res){
        try {
            const murid = await Student.findAll({
                include: ['activity', {
                    model: activity,
                    as: 'activity',
                    attributes: [
                        "id", "name", "schedule"
                    ],
                    through: {
                        attributes: [],
                    }

                }

                ]
            })
            res.status(200).send(murid)
        }
        catch (error) {
            console.log(error)
            res.status(500).send("error gaiss")
        }
    },
    async getStudentById(req, res){
        try {
            const student = await Student.findOne({
                where : {
                    id : req.params.id
                },
                include: ['activity', {
                    model: activity,
                    as: 'activity',
                    attributes: [
                        "id", "name", "schedule"
                    ],
                    through: {
                        attributes: [],
                    }
                }
                ]
            })
            res.status(200).send(student)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async updateStudent(req, res){
        try {
            const student = await Student.update(
            {
                name: req.body.name,
                email: req.body.email,
                school_id: req.body.school_id,
                password: req.body.password
            }, 
            {
                where: {
                    id: req.params.id
                },
            }
            )
            res.status(200).send({
                message: "Update succes!",
                student
            })
        } catch (error) {
            console.log(error)
            res.status(500).send({
                message: "ERROR!"
            })
        }
    },
    async deleteStudent(req, res){
        try {
            const student = await Student.destroy({
                where: {
                    id: req.params.id
                }
            })
            res.status(200).send({
                message: "Delete succes!",
            })
        } catch (error) {
            console.log(error)
            res.status(500).send({
                message: "ERROR!"
            })
        }
    },
    async addStudent(req, res){
        try {
            const student = await Student.create({
                name: req.body.name,
                email: req.body.email,
                school_id: req.body.school_id,
                password: req.body.password
            })
            res.status(200).send({
                message: "New student created",
                student
            })
        } catch (error) {
            console.log(error)
            res.status(500).send({
                message: "Catch ERROR!"
            })
        }
    }
}
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
      await queryInterface.bulkInsert('activities', 
      [ 
      {
        name: 'Tennis',
        schedule: '2020-01-01',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
      {
        name: 'Foot ball',
        schedule: '2020-01-01',
        createdAt: new Date(),
        updatedAt: new Date(),
      }], 
      
      {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
      await queryInterface.bulkDelete('activities', null, {});
     */
  }
};

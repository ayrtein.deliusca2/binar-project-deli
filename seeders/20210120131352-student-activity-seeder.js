'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     */ Example:
      await queryInterface.bulkInsert('studentactivities', 
      [
        {
          student_id: 2,
          activity_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
      },
      {
        student_id: 2,
        activity_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        student_id: 2,
          activity_id: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
      },
      {
        student_id: 3,
          activity_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
      }
    
    ], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     */ Example:
     await queryInterface.bulkDelete('studentactivities', null, {});
     
  }
};

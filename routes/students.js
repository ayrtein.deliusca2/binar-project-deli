var express = require("express");
var router = express.Router();
const passport = require("passport");

const studentControllers = require('../controllers/studentControllers')


router.get(
    "/", 
    passport.authenticate('jwt', { 'session': false }),
    studentControllers.getStudentlist
    );

router.get('/:id', studentControllers.getStudentById)
router.put('/:id', studentControllers.updateStudent)
router.delete('/:id', studentControllers.deleteStudent)
router.post("/", studentControllers.addStudent);


module.exports = router;
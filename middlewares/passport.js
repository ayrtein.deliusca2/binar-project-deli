const passport = require("passport");
const passportJWT = require("passport-jwt");

const JwtStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: '12345',
    },
    function (jwtPayload, done) {
      try {
        return done(null, jwtPayload.student);
      } catch (error) {
        return done(error);
      }
    }
  )
);

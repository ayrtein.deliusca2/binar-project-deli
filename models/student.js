'use strict';
const bcrypt = require('bcrypt');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.school, {
        foreignKey: 'school_id',
        as: 'school',
      });
      this.belongsToMany(models.activity, {
        through: 'studentactivity',
        foreignKey: 'student_id',
        as: 'activity',
      })
    }
  };
  student.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    school_id: DataTypes.INTEGER,
    password: {
      type: DataTypes.STRING,
      set(password){
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds)
        const passwordHashed = bcrypt.hashSync(password, salt)
        this.setDataValue('password', passwordHashed)
      }
    },
  }, {
    sequelize,
    modelName: 'student',
  });
  return student;
};
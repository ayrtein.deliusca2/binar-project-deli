const JWT = require('jsonwebtoken');
const studentModel = require('../models').student;
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    async login(req, res) {
        try {
        const email = req.body.email
        const pasword = req.body.password

        const student = await studentModel.findOne({
            where: {
                email: email
            }
        })
        if (student === null) {
            res.status(400).send({
                message: "Email is wrong"
            })
        }

        
        const comparePassword = bcrypt.compareSync(
            req.body.password, student.password)

        if (!comparePassword){
            res.status(401).send({
                message: 'Password is wrong'
            })
        }
        
        const token = JWT.sign(
            {student},
            '12345'
        )
        res.status(200).send({
            message: "Email is valid",
            token
        })
    } catch (error) {
        console.log('error', error)
        res.status(500).send({
            message: "Error!!"
        })
    }
}
}